
function readURL(event){
  var getImagePath = URL.createObjectURL(event.target.files[0]);
  $('.background').css('background-image', 'url(' + getImagePath + ')');
  $('.picture').css('background-image', 'url(' + getImagePath + ')');
  $('.picture').addClass('shoot');
}

$('.details').click(readURL());
